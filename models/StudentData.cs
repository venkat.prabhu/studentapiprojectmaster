﻿namespace StudentAPI.models
{
    public class StudentData
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public int Age { get; set; }

        public string City { get; set; }
    }
}
