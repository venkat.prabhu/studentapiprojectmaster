﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using StudentAPI.models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace StudentAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StudentController : ControllerBase
    {
        private List<StudentData> students = new List<StudentData>()
       {
                new StudentData(){
                    Id = 1,
                    Name = "joseph",
                    Age = 23,
                    City ="Madurai"
                },
                new StudentData()
                {
                    Id = 2,
                    Name = "venkat",
                    Age = 21,
                    City ="Chennai"
                },
                new StudentData()
                {
                    Id = 3,
                    Name = "love",
                    Age = 25,
                    City ="Coimbatore"
                }
        };
           

        [HttpGet("students")]
        public async Task<IActionResult> GetStudents()
        {
            return Ok(students);
        }

        [HttpGet("students/id")]
        public async Task<IActionResult> GetStudentById(int id)
        {
            var student = students.Find(x=>x.Id == id);
            if (student == null)
            {
                return BadRequest("NO DATA FOUND");
            }
            return Ok(student);
        }

        [HttpPost("AddStudent")]
        public async Task<IActionResult> AddStudent([FromBody]StudentData request)
        {
            students.Add(request);
            return Ok(students);
        }

        [HttpPut("UpdateStudent")]
        public async Task<IActionResult> UpdateStudent([FromForm]StudentData request)
        {
            var student = students.Find(x => x.Id == request.Id);
            if (student == null)
            {
                return BadRequest("NO DATA FOUND");
            }
            student.Name = request.Name;
            student.Age = request.Age;
            student.City = request.City;
            return Ok(students);
        }


        [HttpDelete("DeleteStudent")]
        public async Task<IActionResult> DeleteStudent([FromForm] int id)
        {
            var student = students.Find(x => x.Id == id);
            if (student == null)
            {
                return BadRequest("NO DATA FOUND");
            }
            students.Remove(student);
            return Ok(students);
        }

    }
}
